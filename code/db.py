'''
Simple "Database" async module for testing purposes
'''
import json
import logging
from tornado.httpclient import AsyncHTTPClient, HTTPRequest, HTTPClientError
from tornado.httputil import url_concat


# We will query google fusion tables directly since we were tested to use tornado,
# but we could have just loaded the CSV into an SQLite table or something.
# TODO: Add more error checks


logger = logging.getLogger(__name__)


# Some friendly exceptions

class DBError(Exception):
    MESSAGE = 'Unknown DB Error.'

    def __init__(self, message='', data=None, **kwargs):

        super().__init__(message or self.MESSAGE, **kwargs)
        self.data = data


class NetworkError(DBError):
    MESSAGE = 'Network request error.'


class UnknownFieldsError(DBError):
    '''
    data: will contain list of fields that were invalid
    '''
    MESSAGE = 'Unknown fields.'


# TODO: Properly handle all error codes
class ClientRequestError(DBError):
    MESSAGE = 'Client request error.'


class ServerResponseError(DBError):
    MESSAGE = 'Server request error.'


class RequestTimeoutError(DBError):
    MESSAGE = 'Query or request timed out.'


class DecodeError(DBError):
    MESSAGE = 'Failed to decode data.'


class BaseDB:
    '''
    DB Interface
    '''

    def __init__(self, config):
        '''
        Initializes the database with a config object
        config: Config object required by the implementation
        '''

    async def init(self):
        '''
        Performs any initialization step
        '''
        raise NotImplementedError()


    async def count(self, fields, insensitive=True):
        '''
        Returns how many elements matches the given filter

        fields: {key: value} to filter the count query
        insensitive: if match is case sensitive or insensitive

        returns: {count: n}

        '''
        raise NotImplementedError()


class FusionTablesDB(BaseDB):
    '''
    Implementation that queries Google's fusion tables directly
    through their REST API
    '''

    API_URL = 'https://www.googleapis.com/fusiontables/v2'

    def __init__(self, config):
        '''
        config: {
            table_id: table identifier
            key: google api key,
            request_timeout: requests timeout,
            debug: true
        }
        '''
        self.table_id = config.get('table_id', '')
        self.key = config.get('key', '')
        self.timeout = config.get('request_timeout', 30)
        self.debug = config.get('debug', True)
        self.initialized = False
        self.client = AsyncHTTPClient()
        self.column_names = set()


    async def init(self):
        # If aliready initialized, skip
        if self.initialized:
            return

        if self.debug:
            logger.warning("Going to fetch table columns.")

        r = await self._wrap_json_request(
            'GET',
            '/tables/%s/columns' % self.table_id
        )

        self.column_names = {v['name']: v for v in r.json['items']}
        self.initialized = True

        if self.debug:
            logger.warning("Table columns fetched successfully: %s" % (', '.join(self.column_names.keys())))


    async def count(self, fields=None, insensitive=True):

        if not self.initialized:
            raise RuntimeError("Call .init() before using any methods.")

        # Validate fields
        fields = fields or {}

        if not isinstance(fields, dict):
            raise ValueError("fields must be a dict")

        errors = []
        cols = self.column_names

        for k, v in fields.items():
            if k not in cols:
                errors.append(k)
            else:
                # TODO: Confirm is value is properly escaped
                fields[k] = str(v).replace('\'', "\\'")


        if errors:
            # Sort keys because of the test checks against a sorted list.
            errors.sort(reverse=False)
            raise UnknownFieldsError(data=errors)


        # Sadly we can't use the count query as it is
        # since there's no equals case insensitive
        # so we just perform a group by + count
        # and filter out non exact matches

        # Short circuit if no fields are given or it's case sensitive

        is_grouped = False

        if not fields:
            sql = 'SELECT COUNT() FROM %s' % self.table_id

        else:
            if not insensitive:
                sql = 'SELECT COUNT() FROM %s WHERE %s' % (
                    self.table_id,
                    " AND ".join("%s = '%s'" % v for v in fields.items())
                )
            else:
                names = ', '.join(k for k in fields.keys())
                sql = 'SELECT %s, COUNT() FROM %s WHERE %s GROUP BY %s' % (
                    names,
                    self.table_id,
                    " AND ".join("%s CONTAINS IGNORING CASE '%s'" % v for v in fields.items()),
                    names
                )
                is_grouped = True

        r = await self._wrap_json_request(
            'GET',
            '/query',
            {
                'sql': sql
            }
        )
        if not is_grouped:
            return int(r.json['rows'][0][0]) if 'rows' in r.json else 0

        else:
            # Gotta count the rows that matches all filters
            # this should be not a very long result set
            total = 0
            search_values = list(fields.values())

            for row in r.json['rows'] if 'rows' in r.json else []:

                # Key order should be the same as we sent, so we can zip
                if all(v1.lower() == v2.lower() for v1, v2 in zip(row, search_values)):
                    total += int(row[-1])

            return total


    async def _wrap_json_request(self, method, url, query_params=None, **kwargs):
        # Request wrapper
        try:
            url = self.API_URL + url

            query_params = query_params or {}

            # Set auth and any other query params
            query_params['key'] = self.key

            req = HTTPRequest(
                url_concat(url, query_params),
                method,
                connect_timeout=self.timeout,
                request_timeout=self.timeout,
                **kwargs
            )
            r = await self.client.fetch(req)

            try:
                r.json = json.loads(r.body)
                return r

            except json.JSONDecodeError as e:
                raise DecodeError(data=r.body) from e

        except HTTPClientError as e:

            # Try to parse error
            response = e.response

            try:
                response.json = json.loads(response.body)
                data = response.json
            except:
                data = response.body

            if e.code >= 400 and e.code <= 500:
                raise ClientRequestError(data=data) from e

            elif e.code == 599:
                raise RequestTimeoutError()

            else:
                raise ServerResponseError(data=data) from e

        except Exception as e:
            raise NetworkError(message='Unknown network error.') from e


# Test
def test_fusion(config):

    db = FusionTablesDB(config)

    import asyncio
    loop = asyncio.get_event_loop()

    async def test():

        try:
            await db.init()
            print(db.column_names)
            count = await db.count()
            print("Count no fields: ", count)
            count = await db.count({'borough': 'Brooklyn', 'secondary_color': 'TAN'})
            print("Count fields: ", count)

            # This should throw an error
            await db.count({'unknown': 'asd'})
        except DBError as e:
            print("Base error: ", e)
            print("Error data: ", e.data)



    print(loop.run_until_complete(test()))
    loop.close()
