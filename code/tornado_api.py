from traceback import format_tb
from code.db import DBError, UnknownFieldsError, ClientRequestError
from tornado.web import RequestHandler, Application, HTTPError



class BaseHandler(RequestHandler):

    def initialize(self, db, debug):
        self.db = db
        self.debug = debug

    def set_default_headers(self):
        # Remove server banners and add CORS headers in case we need them.
        self.clear_header('Server')
        self.add_header('Access-Control-Allow-Origin', '*')
        self.add_header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS')
        self.add_header('Access-Control-Max-Age', 3600)


    # Monkey patch this to strip the utf-8 from the json type
    # since the test is not implemented correctly and won't like
    # this otherwise valid header
    def set_header(self, name, value):
        if value == "application/json; charset=UTF-8":
            value = "application/json"

        super().set_header(name, value)


    # Custom error handler
    def write_error(self, status_code, **kwargs):
        err_cls, err, traceback = kwargs['exc_info']

        # "Safe" exceptions, return message and args
        if isinstance(err, HTTPError):
            self.write({
                'code': err.status_code,
                'message': err.log_message,
                'data': err.args
            })
            self.set_status(err.status_code)

        else:
            # Unknown exceptions, log and hide potentially dangerous
            # error messages
            if self.debug:
                self.write({
                    'code': 500,
                    'message': str(err),
                    'data': ''.join(format_tb(traceback))
                })
                self.set_status(500)

            else:
                self.write({
                    'code': 500,
                    'message': "Unexpected error.",
                    'data': None
                })
                self.set_status(500)


    async def prepare(self):
        # Likely to be a race condition here if all requests
        # arrive at the same time and are awaiting
        # for initialization, but we will accept it.
        await self.db.init()



class MainHandler(BaseHandler):
    def get(self, *args, **kwargs):
        # Already sets content type
        self.write({"message": "It's working"})


class CountHandler(BaseHandler):
    async def get(self, *args, **kwargs):
        try:
            args = {name: self.get_query_argument(name) for name in self.request.arguments.keys()}
            res = await self.db.count(args)
            self.write({
                'count': res
            })

        except UnknownFieldsError as e:

            # We should just raise HTTPError as well so all errors
            # follow the same pattern, but since it was requested
            # to return a specific key for the tests to work, we do this.

            self.write({
                'code': 400,
                'message': str(e),
                'data': e.data,

                # This shouldn't go at all
                'unknown fields': e.data
            })
            self.set_status(400)

        except DBError as e:
            raise HTTPError(400, str(e), e.data)

        # Other exceptions handled automatically as unknown


def make_app(db, debug):
    '''
    Creates the tornado app

    db: BaseDB instance to be used
    debug: if True, all error messages will be returned
    '''

    extras = {
        'db': db,
        'debug': debug
    }

    return Application([
        (r"/", MainHandler, extras),
        (r"/count", CountHandler, extras)
    ], debug=debug)
