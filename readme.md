# Test code for getready.io



## Requirements
    Python 3.6
    pip
    virtualenv


## Installation
    Note: Ensure virtualenv points to python3 installation

    virtualenv ./env
        or virtualenv3

    source ./env/bin/activate
        or on Windows: env\Scripts\activate.bat

    pip install -r requirements.txt

    dev:
        pip install pylint
        Optional: Setup visual studio code interpreter


## Running the web server locally
    Run: python main.py
        to start the tornado server


## Running the tests
    Run: python main.py
        don't forget to export READY_TEST_BASE_URL
