#!/usr/bin/env python
from code import db
from code.tornado_api import make_app
import tornado.ioloop


debug = True

config = {
    # Google API key and fusion table id
    'key': '',
    'table_id': '1pKcxc8kzJbBVzLu_kgzoAMzqYhZyUhtScXjB0BQ',
    'debug': debug
}

database = db.FusionTablesDB(config)

if __name__ == '__main__':
    #db.test_fusion(config)

    # Tornado server
    app = make_app(database, debug)
    app.listen(8000)
    print("Listening on port 8000")
    tornado.ioloop.IOLoop.current().start()
